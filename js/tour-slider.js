
let slides = document.querySelectorAll('#slides-tour .tour-item');

let currentSlide = 0;

let slideInterval = setInterval(nextSlide,4000);

function nextSlide() {

    slides[currentSlide].className = 'slide';

    currentSlide = (currentSlide+1)%slides.length;

    slides[currentSlide].className = 'slide move-right';

}