
let slides = document.querySelectorAll('#slides-header .slide');
let switchColor = document.querySelectorAll('#switch .switch-item');

let currentSlide = 0;
let currentSwitch = 0;

let slideInterval = setInterval(nextSlide,4000);
 
function nextSlide() {
    switchColor[currentSwitch].className ='switch-item';
    slides[currentSlide].className = 'slide';

    currentSlide = (currentSlide+1)%slides.length;
    currentSwitch = (currentSwitch+1)%switchColor.length;

    slides[currentSlide].className = 'slide showing';
    switchColor[currentSwitch].className ='switch-item showing-switch';


}